<?php

use Illuminate\Database\Seeder;
use App\User;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create([
            'name' => 'codemau5',
            'email' => 'code.bit.mau@gmail.com',
            'email_verified_at' => now(),
            'password' => bcrypt('red-hat_4944'), // password
            'remember_token' => Str::random(10),
            'cedula' => '58944475',
            'address' => 'P. sherman wllabit Street 23452, Sidney',
            'phone' => '6241640107',
            'role' => 'admin'
        ]);
        factory(User::class, 50)->create();
    }
}
