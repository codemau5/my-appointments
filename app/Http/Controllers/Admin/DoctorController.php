<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\User;

use App\Http\Controllers\Controller;


class DoctorController extends Controller
{

    public function __construct(){
        $this->middleware('auth');
    }

    public function index()
    {
        $doctors= User::doctors()->get();
        return view('doctors.index',compact('doctors'));
    }


    public function create()
    {
        return view('doctors.create');
    }


    public function store(Request $request)
    {
        $rules= [
            'name'      =>  'required|min:3',
            'email'     =>  'required|email',
            'cedula'    =>  'nullable|digits:8',
            'address'   =>  'nullable|min:5',
            'phone'     =>  'nullable|min:6'
        ];

        $this->validate($request,$rules);

        User::create(
            $request->only('name','email','cedula','address','phone') + [ 'role'   =>  'doctor', 'password' => bcrypt($request->password) ]
        );

        $notification= 'El médico se ha registrado correctamente';

        return redirect('/doctors')->with(compact('notification'));
    }

    public function show($id)
    {
        //
    }

 
    public function edit($id)
    {

        $doctor= User::findOrFail($id);

        return view('doctors.edit',compact('doctor'));
    }

 
    public function update(Request $request, $id)
    {
        $rules= [
            'name'      =>  'required|min:3',
            'email'     =>  'required|email',
            'cedula'    =>  'nullable|digits:8',
            'address'   =>  'nullable|min:5',
            'phone'     =>  'nullable|min:6'
        ];
        $this->validate($request,$rules);

        $user= User::findOrFail($id);
        $data= $request->only('name','email','cedula','address','phone');
        $password= $request->password;

        if($password){
            $data['password']= bcrypt($password);
        }

        $user->fill($data);
        $user->save();

        $notification= 'La información del médico se ha actualizado correctamente';

        return redirect('/doctors')->with(compact('notification'));
    }


    public function destroy(User $doctor)
    {
        $doctorName= $doctor->name;
        $doctor->delete();

        $notificationDeleted= 'El médico '.$doctorName.' ha sido eliminado correctamente';

        return redirect('/doctors')->with(compact('notificationDeleted'));
    }
}
