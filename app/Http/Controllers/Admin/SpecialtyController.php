<?php

namespace App\Http\Controllers\Admin;


use Illuminate\Http\Request;
use App\Specialty;

use App\Http\Controllers\Controller;

class SpecialtyController extends Controller
{


    public function index(){
        $specialties= Specialty::all();
        return view('specialties.index',compact('specialties'));
    }

    public function create(){
        return view('specialties.create');
    }

    /*** Funciones custom */
    private function performValidation(Request $request){
        $rules= [
            'name'  =>  'required|min:3',
            'description'  => 'required|min:6'
        ];
        $messages= [
            'name.required'=>'Es necesario ingresar un nombre',
            'name.min'=>'Como minimo el nombre debe tener 3 caracteres'
        ];

        $this->validate($request,$rules,$messages);
    }

    public function store(Request $request){
        //dd($request->all());
        $this->performValidation($request);

        $specialty= new Specialty();

        $specialty->name= $request->name;
        $specialty->description= $request->description;
        $specialty->save();

        $notification= 'La especialidad se ha registrado correctamente.';

        return redirect('/specialties')->with(compact('notification'));
    }

    public function edit(Specialty $specialty){
        return view('specialties.edit',compact('specialty'));
    }

    public function update(Request $request, Specialty $specialty){
             //dd($request->all());
        $this->performValidation($request);

        $specialty->name= $request->name;
        $specialty->description= $request->description;
        $specialty->save();

        $notification= 'La especialidad se ha actualizado correctamente.';

        return redirect('/specialties')->with(compact('notification'));
    }
    public function destroy(Specialty $specialty){
        $deletedName= $specialty->name;
        $specialty->delete();

        $notificationDeleted= 'La especialidad '.$deletedName.' se ha eliminado correctamente';

        return redirect('/specialties')->with(compact('notificationDeleted'));
    }


    
}
