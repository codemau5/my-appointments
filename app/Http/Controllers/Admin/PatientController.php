<?php

namespace App\Http\Controllers\Admin;


use Illuminate\Http\Request;
use App\User;

use App\Http\Controllers\Controller;

class PatientController extends Controller
{

    public function __construct(){
        $this->middleware('auth');
    }

    public function index()
    {
        $patients= User::patients()->paginate(8);
        return view('patients.index',compact('patients'));
    }


    public function create()
    {
        return view('patients.create');
    }


    public function store(Request $request)
    {
        $rules= [
            'name'      =>  'required|min:3',
            'email'     =>  'required|email',
            'cedula'    =>  'nullable|digits:8',
            'address'   =>  'nullable|min:5',
            'phone'     =>  'nullable|min:6'
        ];

        $this->validate($request,$rules);

        User::create(
            $request->only('name','email','address','phone') + [ 'cedula'=>'00000000', 'role'   =>  'patient', 'password' => bcrypt($request->password) ]
        );

        $notification= 'El paciente se ha registrado correctamente';

        return redirect('/patients')->with(compact('notification'));
    }


    public function show($id)
    {
        //
    }


    public function edit($id)
    {
        $patient= User::findOrFail($id);

        return view('patients.edit',compact('patient'));
    }


    public function update(Request $request, $id)
    {
        $rules= [
            'name'      =>  'required|min:3',
            'email'     =>  'required|email',
            'address'   =>  'nullable|min:5',
            'phone'     =>  'nullable|min:6'
        ];
        $this->validate($request,$rules);

        $user= User::findOrFail($id);
        $data= $request->only('name','email','address','phone');
        $password= $request->password;

        if($password){
            $data['password']= bcrypt($password);
        }

        $user->fill($data);
        $user->save();

        $notification= 'La información del paciente se ha actualizado correctamente';

        return redirect('/patients')->with(compact('notification'));
    }


    public function destroy(User $patient)
    {
        $patientName= $patient->name;
        $patient->delete();

        $notificationDeleted= "El paciente $patientName se ha eliminado correctamente.";

        return redirect('/patients')->with(compact('notificationDeleted'));
    }
}
