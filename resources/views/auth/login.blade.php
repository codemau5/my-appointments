@extends('layouts.form')

@section('title', 'Inicio de sesión')
@section('subtitle','Ingresa tus datos para iniciar sesión')

@section('content')
     <!-- Page content -->
    <div class="container mt--8 pb-5">
      <div class="row justify-content-center">
        <div class="col-lg-5 col-md-7">
          <div class="card bg-secondary shadow border-0">
            <div class="card-body px-lg-5 py-lg-5">
              <form method="POST" action="{{route('login')}}">
                @if($errors->any())
                  <div class="text-center text-muted mb-4" role="alert">
                    <small>Oops!! Se encontró un error.</small>
                  </div>
                  <div class="alert alert-danger">
                      {{ $errors->first() }}
                  </div>
               
                @endif

                @csrf
                <div class="form-group mb-3">
                  <div class="input-group input-group-alternative">
                    <div class="input-group-prepend">
                      <span class="input-group-text"><i class="ni ni-email-83"></i></span>
                    </div>
                    <input class="form-control" name="email" value="{{ old('email') }}" placeholder="Email" type="email" required autofocus>
                  </div>
                </div>
                <div class="form-group">
                  <div class="input-group input-group-alternative">
                    <div class="input-group-prepend">
                      <span class="input-group-text"><i class="ni ni-lock-circle-open"></i></span>
                    </div>
                    <input class="form-control" placeholder="Contraseña" name="password" type="password" required>
                  </div>
                </div>
                <div class="custom-control custom-control-alternative custom-checkbox">
                  <input class="custom-control-input" id=" customCheckLogin" type="checkbox">
                  <label name="remember" class="custom-control-label" for=" customCheckLogin" {{ old('remember') ? 'chequed' : '' }}>
                    <span class="text-muted">Recordar sesión</span>
                  </label>
                </div>
                <div class="text-center">
                  <button type="submit" class="btn btn-primary my-4">Ingresar</button>
                </div>
              </form>
            </div>
          </div>
          <div class="row mt-3">
            <div class="col-6">
              @if (Route::has('password.request'))
                  <a class="text-light" href="{{ route('password.request') }}">
                      <small>{{ __('Olvidaste tu contraseña?') }}</small>
                  </a>
              @endif
            </div>
            <div class="col-6 text-right">
              <a href="{{ route('register') }}" class="text-light"><small>Aun no te has registrado?</small></a>
            </div>
          </div>
        </div>
      </div>
    </div>
@endsection