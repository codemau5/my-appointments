@extends('layouts.panel')

@section('specific-styles')
    <style>
        .field-icon {
            float: right;
            margin-right: 11px;
            margin-top: -29px;
            position: relative;
            z-index: 2;
        }

    </style>
@endsection

@section('content')

    <div class="card shadow">
        <div class="card-header border-0">
            <div class="row align-items-center">
                <div class="col">
                    <h3 class="mb-0">Editar médico</h3>
                </div>
                <div class="col text-right">
                <a href="{{ url('doctors') }}" class="btn btn-sm btn-default">Cancelar y volver</a>
                </div>
            </div>
        </div>
        <div class="card-body">
            
                @if($errors->any())
                    <div class="alert alert-danger" role="alert">
                        <ul class="m-0 p-0">
                            @foreach ($errors->all() as $error)
                                <li style="list-style:none">{{$error}}</li>
                            @endforeach
                            
                        </ul>
                    </div>
                @endif
            
            <form action="{{url('doctors/'.$doctor->id)}}" method="POST">
                <div class="form-group">
                    <label for="name">Nombre del médico</label>
                    <input type="text" name="name" class="form-control" value="{{ old('name', $doctor->name) }}" required>
                </div>
                <div class="form-group">
                    <label for="email">E-mail</label>
                    <input type="email" name="email" class="form-control" value="{{ old('email', $doctor->email) }}" required>
                </div>
                <div class="form-group">
                    <label for="cedula">Cédula</label>
                    <input type="text" name="cedula" class="form-control" value="{{ old('cedula', $doctor->cedula) }}" required>
                </div>
                <div class="form-group">
                    <label for="address">Dirección</label>
                    <input type="text" name="address" class="form-control" value="{{ old('address', $doctor->adress) }}" required>
                </div>
                <div class="form-group">
                    <label for="phone">Teléfono / móvil</label>
                    <input type="phone" name="phone" class="form-control" value="{{ old('phone', $doctor->phone) }}" required>
                </div>
                <div class="form-group">
                    <label for="password">Contraseña</label>
                    <input type="text" name="password" class="form-control" id="password-field" value="{{ old('password') }}">
                    
                    <span toggle="#password-field" class="fa fa-fw fa-eye field-icon toggle-password"></span>
                    <p><em>Ingrese un valor sólo si desea modificar la contraseña</em></p>
                </div>
                <button type="submit" class="btn btn-primary">Guardar</button>
                @csrf
                @method('PUT')
            </form>
        </div>
    </div>
</div>
@endsection

@section('footer-scripts')
    <script>
        $(".toggle-password").click(function() {

        $(this).toggleClass("fa-eye fa-eye-slash");
            var input = $($(this).attr("toggle"));

            if (input.attr("type") == "password") {
                input.attr("type", "text");
            } else {
                input.attr("type", "password");
            }

        });
    </script>
@endsection