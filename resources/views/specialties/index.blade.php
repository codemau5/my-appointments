@extends('layouts.panel')

@section('content')

    <div class="card shadow">
        <div class="card-header border-0">
            <div class="row align-items-center">
            <div class="col">
                <h3 class="mb-0">Especialidades</h3>
            </div>
            <div class="col text-right">
            <a href="{{ url('specialties/create') }}" class="btn btn-sm btn-success">Nueva especialidad</a>
            </div>
            </div>
        </div>
        @if(session('notification'))
            <div class="card-body">
                <div class="alert alert-success" role="alert">
                    <strong>{{ session('notification') }}</strong>
                </div>
            </div>
        @endif
        @if(session('notificationDeleted'))
            <div class="card-body">
                <div class="alert alert-warning" role="alert">
                    <strong>{{ session('notificationDeleted') }}</strong>
                </div>
            </div>
        @endif
        <div class="table-responsive">
            <!-- Projects table -->
            <table class="table align-items-center table-flush">
                <thead class="thead-light">
                    <tr>
                        <th scope="col">Nombre</th>
                        <th scope="col">Descripción</th>
                        <th scope="col">Opciones</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($specialties as $specialty)
                        <tr>
                            <th scope="row">
                                {{$specialty->name}}
                            </th>
                            <td>
                                {{$specialty->description}}
                            </td>
                        
                            <td>
                                <form action="{{url('specialties/'.$specialty->id.'/delete')}}" method="POST">
                                    <a href="{{ url('specialties/'.$specialty->id.'/edit') }}" class="btn btn-sm btn-primary">Editar</a>
                                    <button class="btn btn-sm btn-danger" type="submit">Eliminar</button>
                                    @method('DELETE')
                                    @csrf
                                </form>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
@endsection
